# Use uma imagem base do Ubuntu
FROM ubuntu:20.04

# Defina o DEBIAN_FRONTEND como não interativo
ENV DEBIAN_FRONTEND=noninteractive

# Instale o Apache2
RUN apt-get update && apt-get install -y apache2 tzdata

# Copie os arquivos do site para o diretório do Apache
COPY app/ /var/www/html/

# Exponha a porta 80
EXPOSE 80

# Inicie o Apache em segundo plano
CMD ["apache2ctl", "-D", "FOREGROUND"]
