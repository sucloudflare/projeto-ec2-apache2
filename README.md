
<h1>Atualização do servidor apache2 usando docker</h1>

<h1>Configuração do Docker para o GitLab CI/CD</h1>

<p>Este guia descreve os passos para instalar o Docker e configurar as permissões adequadas para o gitlab-runner em um ambiente Linux.</p>

<h2>Instalação do Docker</h2>

<h3>Passo 1: Atualize os pacotes existentes</h3>
    <pre><code>sudo apt update</code></pre>

<h3>Passo 2: Instale as dependências necessárias para adicionar um repositório HTTPS</h3>
    <pre><code>sudo apt install apt-transport-https ca-certificates curl software-properties-common</code></pre>

<h3>Passo 3: Adicione a chave GPG oficial do Docker</h3>
    <pre><code>curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -</code></pre>

 <h3>Passo 4: Adicione o repositório do Docker às fontes APT</h3>
    <pre><code>sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"</code></pre>

<h3>Passo 5: Atualize os pacotes do sistema e instale o Docker CE</h3>
    <pre><code>sudo apt update</code><br><code>sudo apt install docker-ce</code></pre>

 <h3>Passo 6: Verifique se o Docker foi instalado corretamente</h3>
    <pre><code>sudo docker --version</code></pre>

<h2>Configuração das Permissões do Docker para o gitlab-runner</h2>

<h3>Passo 1: Adicione o usuário gitlab-runner ao grupo docker</h3>
    <pre><code>sudo usermod -aG docker gitlab-runner</code></pre>

 <h3>Passo 2: Verifique se o usuário gitlab-runner foi adicionado ao grupo docker</h3>
    <pre><code>groups gitlab-runner</code></pre>

<h3>Passo 3: Verifique as permissões do socket do Docker</h3>
    <pre><code>ls -l /var/run/docker.sock</code></pre>

 <h3>Passo 4: Se necessário, ajuste as permissões do socket do Docker</h3>
    <pre><code>sudo chmod 666 /var/run/docker.sock</code></pre>

 <h3>Passo 5: Reinicie o serviço do gitlab-runner para aplicar as alterações</h3>
    <pre><code>sudo systemctl restart gitlab-runner</code></pre>

 <h2>Verificação do Funcionamento</h2>

 <p>Após seguir os passos acima, você pode verificar se o problema de permissão negada ao conectar-se ao socket do Docker foi resolvido.</p>

 <pre><code>sudo systemctl status gitlab-runner</code></pre>

 <p>Se o status do gitlab-runner for "ativo" e não houver erros relacionados às permissões do Docker, então a configuração foi bem-sucedida.</p>

 <p>Este README.md fornece instruções detalhadas para instalar o Docker e configurar as permissões necessárias para o gitlab-runner no ambiente Linux. Certifique-se de adaptar os comandos de acordo com o sistema operacional específico que você está usando.</p>
</body>





<h1>Configurando GitLab com Runner no AWS EC2</h1>

<h2>Sumário</h2>
    <ul>
        <li><a href="#criar-ec2">Criar Instância EC2</a></li>
        <li><a href="#instalar-gitlab">Instalar GitLab em EC2</a></li>
        <li><a href="#registrar-runner">Registrar GitLab Runner</a></li>
        <li><a href="#configurar-pipeline">Configurar Pipeline no GitLab</a></li>
    </ul>

  <h2 id="criar-ec2">1. Criar Instância EC2</h2>
  <img src='./a.png' alt='' >

  <h3>Passo 1: Acessar AWS Console</h3>
    <ul>
        <li>Acesse o <a href="https://aws.amazon.com/console/" target="_blank">AWS Management Console</a>.</li>
    </ul>

  <h3>Passo 2: Lançar uma Instância EC2</h3>
<ul>
        <li>No console da AWS, vá para "EC2" e clique em "Launch Instance".</li>
        <li>Escolha uma Amazon Machine Image (AMI). Para GitLab, uma AMI do Ubuntu é recomendada.</li>
        <li>Selecione o tipo de instância. Para testes e desenvolvimento, uma <code>t2.micro</code> pode ser suficiente.</li>
        <li>Configure as opções de instância conforme necessário, incluindo armazenamento e detalhes da rede.</li>
        <li>Adicione um par de chaves (Key Pair) para acessar a instância via SSH.</li>
        <li>Configure o Security Group para permitir o tráfego HTTP (porta 80) e SSH (porta 22).</li>
        <li>Clique em "Launch".</li>
    </ul>

 <h3>Passo 3: Conectar à Instância EC2</h3>
    <ul>
        <li>Conecte-se à sua instância via SSH:</li>
        <pre><code>ssh -i /path/to/your-key.pem ubuntu@your-ec2-public-dns</code></pre>
    </ul>

<img src='b.png' alt=''>

 <h2 id="instalar-gitlab">2. Instalar GitLab em EC2</h2>

  <h3>Passo 1: Instalar Dependências</h3>
    <ul>
        <li>Atualize os pacotes e instale as dependências necessárias:</li>
        <pre><code>sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl</code></pre>
    </ul>

<h3>Passo 2: Adicionar o Repositório GitLab</h3>
    <ul>
        <li>Adicione o repositório GitLab:</li>
        <pre><code>curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash</code></pre>
    </ul>

<h3>Passo 3: Instalar GitLab</h3>
    <ul>
        <li>Instale GitLab usando o endereço de IP público ou nome de domínio da sua instância:</li>
        <pre><code>sudo EXTERNAL_URL="http://your-ec2-public-dns" apt-get install gitlab-ee</code></pre>
    </ul>

 <h3>Passo 4: Configurar GitLab</h3>
    <ul>
        <li>Configure GitLab executando o comando:</li>
        <pre><code>sudo gitlab-ctl reconfigure</code></pre>
    </ul>

 <h2 id="registrar-runner">3. Registrar GitLab Runner</h2>

 <h3>Passo 1: Instalar GitLab Runner</h3>
    <ul>
        <li>Instale o GitLab Runner:</li>
        <pre><code>curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start</code></pre>
    </ul>

 <h3>Passo 2: Registrar o Runner</h3>
    <ul>
        <li>Registre o runner com seu GitLab:</li>
        <pre><code>sudo gitlab-runner register</code></pre>
        <ul>
            <li>Durante o processo de registro, você precisará fornecer a URL do GitLab, o token de registro (encontrado nas configurações do projeto no GitLab), uma descrição para o runner, etiquetas e selecionar o executor (escolha <code>shell</code>).</li>
        </ul>
    </ul>

<h2 id="configurar-pipeline">4. Configurar Pipeline no GitLab</h2>

 <h3>Passo 1: Criar um Novo Repositório</h3>
    <ul>
        <li>No GitLab, crie um novo repositório ou use um repositório existente.</li>
    </ul>

 <h3>Passo 2: Adicionar <code>.gitlab-ci.yml</code></h3>
    <ul>
        <li>Crie um arquivo <code>.gitlab-ci.yml</code> no repositório com a configuração da pipeline. Exemplo:</li>
        <pre><code>stages:
  - build
  - deploy

build_job:
  stage: build
  script:
    - echo "Building the project..."

deploy_job:
  stage: deploy
  script:
    - echo "Deploying the project..."</code></pre>
    </ul>

 <h3>Passo 3: Commit e Push</h3>
    <ul>
        <li>Commit e push do arquivo <code>.gitlab-ci.yml</code> para o repositório:</li>
        <pre><code>git add .gitlab-ci.yml
git commit -m "Add GitLab CI configuration"
git push origin main</code></pre>
    </ul>

 <h3>Passo 4: Verificar Pipeline</h3>
    <ul>
        <li>No GitLab, navegue até "CI/CD" > "Pipelines" no seu projeto para ver a execução da pipeline.</li>
    </ul>

 <h3>Trocar Usuário de <code>ubuntu@</code> para <code>gitlab@</code></h3>
    <ul>
        <li>Execute o comando abaixo para alterar a propriedade do diretório do Apache para o usuário <code>gitlab-runner</code>:</li>
        <pre><code>sudo chown gitlab-runner /var/www/html -R</code></pre>
    </ul>

<img src='c.png' alt=''>